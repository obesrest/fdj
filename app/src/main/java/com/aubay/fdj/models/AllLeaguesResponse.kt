package com.aubay.fdj.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

class AllLeaguesResponse {

    @SerializedName("leagues")
    @Expose
    var leagueList: List<League> = ArrayList()
}
