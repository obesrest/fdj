package com.aubay.fdj.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class AllPlayersResponse {

    @SerializedName("player")
    @Expose
    var playerList: List<Player> = ArrayList()

}