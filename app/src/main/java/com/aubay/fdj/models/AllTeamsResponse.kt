package com.aubay.fdj.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class AllTeamsResponse {

    @SerializedName("teams")
    @Expose
    var teamList: List<Team> = ArrayList()

}