package com.aubay.fdj.network


import com.aubay.fdj.models.*

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface TheSportsDbApi {

    @GET("all_leagues.php")
    fun allLeagues(): Observable<AllLeaguesResponse>

    @GET("search_all_teams.php")
    fun getAllTeamsInLeague(@Query("l") league: String): Observable<AllTeamsResponse>

    @GET("searchplayers.php")
    fun getAllPlayersInTeam(@Query("t") teamId: String): Observable<AllPlayersResponse>

}
