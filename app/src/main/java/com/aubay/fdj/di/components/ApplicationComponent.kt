package com.aubay.fdj.di.components

import com.aubay.fdj.ApplicationClass
import com.aubay.fdj.di.modules.ApplicationModule
import com.aubay.fdj.di.modules.NetworkModule
import com.aubay.fdj.ui.home.HomePresenterImpl
import com.aubay.fdj.ui.team.TeamPresenterImpl
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class])
interface ApplicationComponent {

    fun inject(mApplication: ApplicationClass)
    fun inject(mHomePresenterImpl: HomePresenterImpl)
    fun inject(mTeamPresenterImpl: TeamPresenterImpl)

}