package com.aubay.fdj.di.modules

import android.app.Application
import com.aubay.fdj.BuildConfig
import com.aubay.fdj.BuildConfig.BASE_URL
import com.aubay.fdj.network.TheSportsDbApi
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {


        @Provides
        @Singleton
        fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
            return Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(getBaseUrl()).build()
        }

        @Provides
        @Singleton
        protected fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            return interceptor
        }

        @Provides
        @Singleton
        fun provideNetworkService(retrofit: Retrofit): TheSportsDbApi {
            return retrofit.create(TheSportsDbApi::class.java)
        }

        @Provides
        @Singleton
        protected fun provideOkHttpClientDefault(interceptor: HttpLoggingInterceptor, cache: Cache): OkHttpClient {
            val okBuilder = OkHttpClient.Builder()
            okBuilder.cache(cache)
            okBuilder.addInterceptor(interceptor)
            okBuilder.addInterceptor { chain ->
                val request = chain.request()
                val builder = request.newBuilder()

                chain.proceed(builder.build())
            }

            return okBuilder.build()

        }

        @Provides
        @Singleton
        protected fun provideOkHttpCache(application: Application): Cache {
            val cacheSize = 10L*1024*1024
            return Cache(application.getCacheDir(), cacheSize)
        }

        protected fun getBaseUrl() : String {
            return BASE_URL + getApiKey() + "/"
        }

        protected fun getApiKey() : String {
            return if(BuildConfig.DEBUG) "1" else /*TODO*/ "some_key"
        }
}