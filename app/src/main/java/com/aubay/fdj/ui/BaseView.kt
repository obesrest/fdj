package com.aubay.fdj.ui

import android.support.annotation.StringRes
import android.view.View
import android.widget.TextView

interface BaseView {

    fun showProgressBar()

    fun showContent()

    fun showError(@StringRes errorResId: Int)

    fun setProgressConfig(mainView: View, progressView: View, errorView: TextView)
}