package com.aubay.fdj.ui.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import com.aubay.fdj.R
import com.aubay.fdj.models.Team
import com.aubay.fdj.ui.BaseActivity
import com.aubay.fdj.ui.adapters.TeamItemAdapter
import com.aubay.fdj.ui.team.TeamActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity<HomePresenterImpl>(),HomeView {

    companion object {
        const val LEAGUE_NAME = "ligue_name"
    }

    var mLeague: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        if (savedInstanceState?.getString(LEAGUE_NAME) != null) {
            mLeague = savedInstanceState.getString(LEAGUE_NAME)
        } else if (intent.getStringExtra(LEAGUE_NAME) != null) {
            mLeague = intent.getStringExtra(LEAGUE_NAME)
        }

        init()
    }

    override fun onResume() {
        super.onResume()
        showContent()
        if (rv_teams.adapter != null && rv_teams.adapter!!.itemCount >= 0) {
            rv_teams.requestFocus()
        }
    }

    fun init() {
        setProgressConfig(rv_teams, progressBar_view, tv_error)
    }

    override fun instantiatePresenter(): HomePresenterImpl {
        return HomePresenterImpl(this, application)
    }

    override val onItemClick: (Team) -> Unit = {team -> presenter?.teamItemClicked(team)}

    override fun showTeamBadges(teamList: List<Team>) {
            rv_teams.layoutManager = GridLayoutManager(this, 2)
            rv_teams.adapter = TeamItemAdapter(teamList, this, onItemClick)
            showContent()
    }

    override fun showAllPlayersInTeam(team: Team) {
        val intent = Intent(this, TeamActivity::class.java)
        intent.putExtra(TeamActivity.TEAM_NAME, team.strTeam)
        intent.putExtra(LEAGUE_NAME, mLeague)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.home_menu, menu)

        val menuItem = menu.findItem(R.id.menu_search)
        (menuItem.actionView as SearchView).setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(league: String?): Boolean {
                showProgressBar()
                mLeague = league
                presenter?.getAllTeamsInLeague(league!!)

                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }

        })
        (menuItem.actionView as SearchView).setQuery(mLeague, true)
        (menuItem.actionView as SearchView).setIconifiedByDefault(false)

        return true
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString(LEAGUE_NAME, mLeague )
        super.onSaveInstanceState(outState)
    }

}
