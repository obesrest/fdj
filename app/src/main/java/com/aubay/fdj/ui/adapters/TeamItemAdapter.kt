package com.aubay.fdj.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aubay.fdj.R
import com.aubay.fdj.models.Team
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.team_item.view.*

class TeamItemAdapter(var teamList: List<Team>, val context: Context, val onTeamClickListener: (Team) -> Unit) : RecyclerView.Adapter<TeamItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.team_item, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return teamList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val team = teamList[position]
        viewHolder.itemView.constraint_layout.setOnClickListener { onTeamClickListener(team)}
        Picasso.with(context)
                .load(team.strTeamBadge)
                .into(viewHolder.teamBadgeView)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var teamBadgeView = view.findViewById<ImageView>(R.id.iv_team_badge)

    }


}