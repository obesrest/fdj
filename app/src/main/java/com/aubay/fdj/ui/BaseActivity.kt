package com.aubay.fdj.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView

abstract class BaseActivity<P : BasePresenter<BaseView>> : AppCompatActivity(), BaseView {

    lateinit var presenter:P

    var mainView: View?= null
    var progressView: View?= null
    var errorView: TextView?= null
    var isProgressConfigured: Boolean=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = instantiatePresenter()
    }

    /**
     * Instantiates the presenter the Activity is based on.
     */
    protected abstract fun instantiatePresenter(): P


    override fun setProgressConfig(mainView: View, progressView: View, errorView: TextView) {
        isProgressConfigured = true
        this.mainView = mainView
        this.progressView = progressView
        this.errorView = errorView
    }

    override fun showProgressBar() {
        if (isProgressConfigured) {
            progressView?.visibility = View.VISIBLE
            errorView?.visibility = View.GONE
            mainView?.visibility = View.GONE
        }
    }

    override fun showContent() {
        if (isProgressConfigured) {
            progressView?.visibility = View.GONE
            errorView?.visibility = View.GONE
            mainView?.visibility = View.VISIBLE
        }
    }

    override fun showError(errorResId: Int) {
        if (isProgressConfigured) {
            errorView?.setText(errorResId)
            progressView?.visibility = View.GONE
            errorView?.visibility = View.VISIBLE
            mainView?.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unbindView()
    }

}