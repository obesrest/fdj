package com.aubay.fdj.ui.home

import android.app.Application
import com.aubay.fdj.ApplicationClass
import com.aubay.fdj.R
import com.aubay.fdj.models.Team
import com.aubay.fdj.network.TheSportsDbApi
import com.aubay.fdj.ui.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class HomePresenterImpl(homeView: HomeView, var applicationComponent: Application) : BasePresenter<HomeView>(homeView), HomePresenter {

    @Inject
    lateinit var mTheSportsDbApi: TheSportsDbApi

    init {
        (applicationComponent as ApplicationClass).applicationComponent.inject(this)
    }

    override fun getAllTeamsInLeague(leagueString: String) {
        val allTeams = mTheSportsDbApi.getAllTeamsInLeague(leagueString)
        addDisposable(allTeams
                .map {allTeamsResponse -> allTeamsResponse.teamList}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { teamList -> view?.showTeamBadges(teamList) },
                        { view?.showError(R.string.error_league_not_found) }
                ))
    }

    override fun teamItemClicked(team: Team) {
        view?.showAllPlayersInTeam(team)
    }

}