package com.aubay.fdj.ui.team

import android.app.Application
import com.aubay.fdj.ApplicationClass
import com.aubay.fdj.R
import com.aubay.fdj.network.TheSportsDbApi
import com.aubay.fdj.ui.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TeamPresenterImpl(teamView: TeamView, var applicationComponent: Application) : TeamPresenter, BasePresenter<TeamView>(teamView){

    @Inject
    lateinit var mTheSportsDbApi: TheSportsDbApi

    init {
        (applicationComponent as ApplicationClass).applicationComponent.inject(this)
    }

    override fun getAllPlayers(teamId: String) {
        val allPlayers = mTheSportsDbApi.getAllPlayersInTeam(teamId)
        addDisposable(allPlayers
                .map {allPlayersResponse -> allPlayersResponse.playerList}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { playerList -> view?.showPlayers(playerList) },
                        { view?.showError(R.string.no_player_found) }
                ))
    }

}