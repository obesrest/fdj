package com.aubay.fdj.ui.team

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager

import com.aubay.fdj.R
import com.aubay.fdj.models.Player
import com.aubay.fdj.ui.BaseActivity
import com.aubay.fdj.ui.adapters.PlayerItemAdapter
import kotlinx.android.synthetic.main.activity_team.*
import android.view.MenuItem
import com.aubay.fdj.ui.home.HomeActivity


class TeamActivity : BaseActivity<TeamPresenterImpl>(), TeamView {

    companion object {
        const val TEAM_NAME = "team_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        val teamId = intent.getStringExtra(TEAM_NAME)
        init(teamId)

    }

    fun init(teamId: String) {
        setTitle(teamId)
        setProgressConfig(recyclerview_players, progressBar_view, tv_error)
        showProgressBar()
        presenter.getAllPlayers(teamId)
    }

    override fun instantiatePresenter(): TeamPresenterImpl {
        return TeamPresenterImpl(this, application)
    }

    override fun showPlayers(playerList: List<Player>) {
        recyclerview_players.layoutManager = LinearLayoutManager(this)
        recyclerview_players.adapter = PlayerItemAdapter(playerList, this)
        showContent()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                val intent = Intent(this, HomeActivity::class.java)
                intent.putExtra(HomeActivity.LEAGUE_NAME, this.intent.getStringExtra(HomeActivity.LEAGUE_NAME))
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
