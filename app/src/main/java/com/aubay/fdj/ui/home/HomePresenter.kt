package com.aubay.fdj.ui.home

import com.aubay.fdj.models.Team

interface HomePresenter {

    fun teamItemClicked(team: Team)

    fun getAllTeamsInLeague(leagueString: String)
}