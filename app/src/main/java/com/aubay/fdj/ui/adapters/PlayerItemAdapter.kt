package com.aubay.fdj.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aubay.fdj.R
import com.aubay.fdj.models.Player
import com.squareup.picasso.Picasso

class PlayerItemAdapter(var playerList: List<Player>, val context: Context) : RecyclerView.Adapter<PlayerItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.player_item, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return playerList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val player = playerList[position]
        val picture = if (player.strCutout == null) player.strThumb else player.strCutout
        if (picture != null) {
            Picasso.with(context)
                    .load(picture)
                    .into(viewHolder.pictureView)
        } else
            viewHolder.pictureView.setImageResource(R.drawable.profile_icon)

        viewHolder.nameView.setText(player.strPlayer)
        viewHolder.positionView.setText(player.strPosition)
        viewHolder.birthView.setText(context.getString(R.string.birth_date, player.dateBorn))
        viewHolder.priceView.setText(player.strSigning)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var pictureView = view.findViewById<ImageView>(R.id.iv_picture)
        var nameView = view.findViewById<TextView>(R.id.tv_name)
        var positionView = view.findViewById<TextView>(R.id.tv_position)
        var birthView = view.findViewById<TextView>(R.id.tv_birth)
        var priceView = view.findViewById<TextView>(R.id.tv_price)
    }
}