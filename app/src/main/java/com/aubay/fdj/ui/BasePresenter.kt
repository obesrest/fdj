package com.aubay.fdj.ui

import android.support.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BasePresenter<out V : BaseView>(val view: V? ) {



    companion object {

        /*
        var compositeDisposables: CompositeDisposable
        Every method which will be part of presenter layer will be added in it so we could dispose off them once they are no more in our use
        */
        var compositeDisposables: CompositeDisposable = CompositeDisposable()

    }


    init {


    }


    protected fun view(): V? {
        return view
    }

    @CallSuper
    fun unbindView() {
        compositeDisposables.clear()
    }

    fun addDisposable(disposable: Disposable) {
        compositeDisposables.add(disposable)
    }


}
