package com.aubay.fdj.ui.team


interface TeamPresenter {

    fun getAllPlayers(teamId: String)

}