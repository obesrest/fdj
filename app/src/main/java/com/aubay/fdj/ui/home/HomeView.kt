package com.aubay.fdj.ui.home

import com.aubay.fdj.models.Team
import com.aubay.fdj.ui.BaseView

interface HomeView : BaseView {

    fun showTeamBadges(teamList: List<Team>)

    fun showAllPlayersInTeam(team: Team)

    val onItemClick: (Team) -> Unit


}