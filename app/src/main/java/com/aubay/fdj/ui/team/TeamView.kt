package com.aubay.fdj.ui.team

import com.aubay.fdj.models.Player
import com.aubay.fdj.ui.BaseView

interface TeamView : BaseView {

    fun showPlayers(playerList: List<Player>)

}