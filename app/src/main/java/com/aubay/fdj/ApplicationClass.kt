package com.aubay.fdj

import android.app.Application
import com.aubay.fdj.di.components.ApplicationComponent
import com.aubay.fdj.di.components.DaggerApplicationComponent
import com.aubay.fdj.di.modules.ApplicationModule
import com.aubay.fdj.di.modules.NetworkModule

open class ApplicationClass : Application() {


    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .networkModule(NetworkModule())
                .build()

        applicationComponent.inject(this)
    }
}